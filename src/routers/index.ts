import { Router } from 'express'

import currenciesRouter from './currencies/index'

const router = Router()

router.use('/v1', currenciesRouter)

export default router
