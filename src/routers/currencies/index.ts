import { Router } from 'express'

import { getAllCurrencies } from './controllers'

const router = Router()

router.get('/all-currencies', getAllCurrencies)

export default router
