import fs from 'fs'

import dotenv from 'dotenv'

import axios from 'axios'

dotenv.config()

export async function getAllCurrencies (request, response): Promise<void> {
  fs.readFile('./src/data/data.json', 'utf8', (error, data) => {
    if (error) {
      return response.message({ message: 'Ошибка при получении данных с сервера', error })
    }
    response.json(JSON.parse(data))
  })
}

export async function updateDataApi (): Promise<any> {
  const requestLink = 'https://api.simpleswap.io/v1/get_all_currencies?api_key='

  try {
    const { data } = await axios.get(`${requestLink}${process.env.API_KEY}`)

    const dataR = {
      currenciesList: [...data],
    }
    fs.writeFile('./src/data/data.json', JSON.stringify(dataR), () => {
      console.log('Данные успешно обновлены')
    })
  } catch (error) {
    console.log(error.response)
  }
}
