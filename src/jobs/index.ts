import { CronJob } from 'cron'

import { updateDataApi } from '../routers/currencies/controllers'

export const jobUpdateData = new CronJob({
  cronTime: '*/5 * * * *',
  runOnInit: true,
  onTick: async () => {
    console.log('on update Data start')
    await updateDataApi()
  },
})

jobUpdateData.start()
