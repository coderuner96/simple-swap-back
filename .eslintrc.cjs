module.exports = {
  extends: [
    'standard-with-typescript',
  ],
  parserOptions: {
    ecmaVersion: 11,
    project: './tsconfig.json',
    parser: "@typescript-eslint/parser"
  },
  rules: {
    camelcase: 'off',
    'comma-dangle': ['error', 'always-multiline'],
    'max-len': ['error', { code: 150 }],
    '@typescript-eslint/no-var-requires': [0],
    '@typescript-eslint/strict-boolean-expressions': [0],
    '@typescript-eslint/consistent-type-assertions': ['error', { assertionStyle: 'angle-bracket' }],
    'no-void': [0],
    'import/order': ['error', {
      'newlines-between': 'always-and-inside-groups',
    }],
  },
}
