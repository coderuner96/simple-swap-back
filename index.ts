import express from 'express'
import cors from 'cors'

import dotenv from 'dotenv'

import router from './src/routers'

const app = express()
dotenv.config()

app.use(cors())
app.use(express.json())
app.use('/api', router)

const { PORT } = process.env

app.listen(PORT, () => console.log(`server started in port ${PORT}`))
